"""ProyectoMisPerris URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from core import views
from django.conf import settings
from django.conf.urls.static import static
from fcm_django.api.rest_framework import FCMDeviceAuthorizedViewSet

from rest_framework import routers

router = routers.DefaultRouter()
router.register('mascotas',views.MascotaViewSet)
router.register('devices',FCMDeviceAuthorizedViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name="home"),
    path('misPerris/', views.misPerris, name="misPerris"),
    path('contacto/', views.contacto, name="contacto"),
    path('crear-mascota/', views.crearMascota, name="crearMascota"),
    path('listar-mascota/', views.listarMascota, name="listarMascota"),
    path('editar-mascota/<id>/', views.editarMascota, name="editarMascota"),
    path('eliminar-mascota/<id>/', views.eliminarMascota, name="eliminarMascota"),
    path('accounts/', include('django.contrib.auth.urls')),
    path('registro/', views.registro, name="registro"),
    path('api/', include(router.urls)),
    path('', include(router.urls)),

    path('accounts/', include('allauth.urls')),

    path('', include('pwa.urls')),

    path('guardar-token/', views.guardar_token, name="guardar_token"),



]

urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)