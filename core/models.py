from django.db import models
from django.utils import timezone
# Create your models here.

class Categoria(models.Model):
    descripcion = models.CharField(max_length=50)
    fecha_crea  = models.DateTimeField(auto_now_add=True)
    fecha_publ  = models.DateTimeField(auto_now=True, null=True)

    def publish(self):
        self.fecha_publ = timezone.now()
        self.save()
    
    def __str__(self):
        return self.descripcion

class Mascota(models.Model):
    nombre = models.CharField(max_length=50)
    observacion = models.TextField()
    categoria = models.ForeignKey(Categoria, on_delete=models.PROTECT)
    fecha_crea  = models.DateTimeField(auto_now_add=True)
    fecha_publ  = models.DateTimeField(auto_now=True, null=True)
    imagen      = models.ImageField(null=True,blank=True)

    def publish(self):
        self.fecha_publ = timezone.now()
        self.save()
    
    def __str__(self):
        return self.nombre

tipo_mensaje = [
    [0, "Solicitud ingresar Perris"],
    [1, "Solicitud adoptar Perris"],
    [2, "Solicitud colaborar con Mis Perris"],
]

class Contacto(models.Model):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    mail = models.EmailField()
    tipo_mensaje = models.IntegerField(choices=tipo_mensaje)
    mensaje = models.TextField()
    avisos = models.BooleanField()

    def __str__(self):
        return self.nombre
    
