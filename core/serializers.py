from rest_framework import serializers
from .models import Mascota

class MascotaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Mascota
        #fields = ['nombre', 'observacion','categoria', 'fecha_crea']
        fields = '__all__'