from django.shortcuts import render, redirect, get_object_or_404
from .models import Mascota
from .forms import ContactoForm, MascotaForm, CustomUserCreationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required, permission_required


from rest_framework import viewsets
from .serializers import MascotaSerializer

# <------------------------------------------------------------
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt

from django.http import HttpResponse, HttpResponseBadRequest
from django.core import serializers
import json


from fcm_django.models import FCMDevice

@csrf_exempt
@require_http_methods(['POST'])
def guardar_token(request):

    body = request.body.decode('utf-8')
    bodyDict = json.loads(body)

    token = bodyDict['token']

    existe = FCMDevice.objects.filter(registration_id = token, active=True)

    if len(existe) > 0:
        return HttpResponseBadRequest(json.dumps({'mensaje':'el token ya existe'}))
    
    dispositivo = FCMDevice()
    dispositivo.registration_id = token
    dispositivo.active = True

    #solo si el usuario esta autenticado procederemos a enlazarlo
    if request.user.is_authenticated:
        dispositivo.user = request.user
    
    try:
        dispositivo.save()
        return HttpResponse(json.dumps({'mensaje':'token guardado'}))
    except:
        return HttpResponseBadRequest(json.dumps({'mensaje':'no se pudo guardar token'}))



# Create your views here.
def home(request):
    return render(request, "core/home.html")

def misPerris(request):
    mascotas = Mascota.objects.all()
    return render(request, "core/misPerris.html", {'perritos': mascotas})

def contacto(request):
    form = ContactoForm()
    data = {'formulario': form}
    if request.method == 'POST':
        formulario = ContactoForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = "Datos ingresados correctamente"
    return render(request, "core/contacto.html", data)


def crearMascota(request):
    form = MascotaForm()
    data = {'formulario':form}
    
    if request.method == 'POST':
        formulario = MascotaForm(request.POST,files=request.FILES)
        if formulario.is_valid():
            formulario.save()

            dispositivos = FCMDevice.objects.all()
            
            dispositivos.send_message(
                title= "Nueva Mascota: " + formulario.cleaned_data["nombre"],
                body=formulario.cleaned_data["observacion"]) 
            data['mensaje'] = "Datos ingresados correctamente"
        else:
            data['formulario'] = formulario
    return render(request,"core/mascota/crear.html", data)

@permission_required('core.view_mascota')
def listarMascota(request):
    mascotas = Mascota.objects.all()

    return render(request,"core/mascota/listar.html", {'perritos': mascotas})

@permission_required('core.change_mascota')
def editarMascota(request, id):
    mascota = get_object_or_404(Mascota, id=id)
    data = {'formulario': MascotaForm(instance=mascota)}

    if request.method == 'POST':
        formulario = MascotaForm(request.POST, instance=mascota, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            return redirect(to="listarMascota")
        else:
            data['formulario'] = formulario

    return render(request,"core/mascota/editar.html", data)

@permission_required('core.delete_mascota')
def eliminarMascota(request, id):
    mascota = get_object_or_404(Mascota, id=id)
    mascota.delete()

    return redirect(to="listarMascota")

def registro(request):
    data = {'form': CustomUserCreationForm()}

    if request.method == 'POST':
        formulario = CustomUserCreationForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            user = authenticate(username=formulario.cleaned_data["username"],password=formulario.cleaned_data["password1"])
            login(request,user)
            return redirect(to=home)
        data['form'] = formulario
    return render(request, "registration/registro.html", data)

class MascotaViewSet(viewsets.ModelViewSet):
    queryset = Mascota.objects.all()
    serializer_class = MascotaSerializer