//llamado api
$(document).ready(function(){
    $("#buscar").click(function(){
        var texto = "";
        var tag = $("#b").val();
        $("#imagenes").text("cargando...");
        $.getJSON("https://www.flickr.com/services/feeds/photos_public.gne?tags=" + tag + "&tagmode=any&format=json&jsoncallback=?", function(datos){
            $.each(datos.items, function(i, item){
                texto += "<div class='cuadro'>";
                texto += "<img src='" + item.media.m + "'/>";
                texto += "</div>";
            });
            $("#imagenes").html(texto);
        });
    })

})