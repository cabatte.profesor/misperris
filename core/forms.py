from django import forms
from django.forms import ModelForm
from .models import Contacto, Mascota
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class ContactoForm(ModelForm):
    class Meta:
        model = Contacto
        fields = '__all__'
        #fields = ["nombre","mail","tipo_mensaje"]

class MascotaForm(ModelForm):
    class Meta:
        model = Mascota
        fields = '__all__'

class CustomUserCreationForm(UserCreationForm):
    pass