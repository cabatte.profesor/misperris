var CACHE_NAME = 'misperris-cache-v1';
var urlsToCache = [
    '/',
    '/static/core/css/estilos.css',
    '/static/core/img/logo.png',
];

self.addEventListener('install', function (event) {
    // Perform install steps
    event.waitUntil(
        caches.open(CACHE_NAME).then(function (cache) {
            console.log('Opened cache');
            return cache.addAll(urlsToCache);
        })
    );
});

/*
self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches.match(event.request).then(function(response) {

          return fetch(event.request)
          .catch(function(rsp) {
             return response; 
          });
          
          
        })
    );
});
*/


self.addEventListener('fetch', function (event) {
    event.respondWith(

        fetch(event.request).then((result) => {
            return caches.open(CACHE_NAME).then(function (c) {
                c.put(event.request.url, result.clone())
                return result;
            })

        }).catch(function (e) {
            return caches.match(event.request)
        })



    );
});





//codigo notificaciones Push
importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-messaging.js');


  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyDSe1qSPXatV5NGHWtRm-iwKuf64e9QKL4",
    authDomain: "proyectomisperris.firebaseapp.com",
    databaseURL: "https://proyectomisperris.firebaseio.com",
    projectId: "proyectomisperris",
    storageBucket: "proyectomisperris.appspot.com",
    messagingSenderId: "475152357839",
    appId: "1:475152357839:web:843051c81386bf7d209aba"
  };
  
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

let messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
    console.log("enviar notificacion");
    let title = "Titulo ServiceWorker";

    let options = {
        body: "Mensaje",
        icon: "/static/core/img/logo.png"
    }

    self.registration.showNotification(title, options);

});
              
